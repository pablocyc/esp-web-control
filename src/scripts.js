let opts = {
  lines: 12, // The number of lines to draw
  angle: 0.00, // The length of each line
  lineWidth: 0.44, // The line thickness
  
  pointer: {
    length: 0.5, // The radius of the inner circle
    strokeWidth: 0.035, // The rotation offset
    color: '#000A3A' // Fill color
  },
  
  limitMax: true,   // If true, the pointer will not go past the end of the gauge
  limitMin: true,
  colorStart: 'blue',   // Colors
  colorStop: '#8FC0DA',    // just experiment with them
  strokeColor: '#E0E0E0',   // to see which ones work best for you
  generateGradient: true,
  
  staticLabels: {
    font: "10px sans-serif",  // Specifies font
    labels: [-100, -80, -60, -40, -20, 0],  // Print labels at these values
    color: "#000000",  // Optional: Label text color
    fractionDigits: 0,  // Optional: Numerical precision. 0=round off.
  },
  
  staticZones: [
   {strokeStyle: "#F03E3E", min: -100, max: -70}, // Red
   {strokeStyle: "#FFDD00", min: -70, max: -60}, // Yellow
   {strokeStyle: "#30B32D", min: -60, max: 0}, // Green
   //{strokeStyle: "#FFDD00", min: 220, max: 260}, // Yellow
   //{strokeStyle: "#F03E3E", min: 260, max: 300}  // Red
  ],
  
  renderTicks: {
    divisions: 10,
    divWidth: 1.1,
    divLength: 0.5,
    divColor: '#333333',
    subDivisions: 2,
    subLength: 0.3,
    subWidth: 0.6,
    subColor: '#666666'
  },
  
};

let target = document.getElementById('gauge'); // your canvas element
let gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
gauge.maxValue = 0.0; // set max gauge value
gauge.setMinValue(-100);  // Prefer setter over gauge.minValue = 0
gauge.animationSpeed = 20; // set animation speed (32 is default value)
gauge.setTextField(document.getElementById("gauge-textfield"));
gauge.set(-100);

function chamar(valor){
  valor=document.getElementById("campo").value;
  gauge.set(valor); // set actual value
}